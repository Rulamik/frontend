$(document).ready(function () {
    var main = $(".main");
    var ESC_KEY_CODE = 27;
    var ENTER_KEY_CODE = 13;
    main.on("click", ".opop", function () {
        $(this).children(".search").css({ "width": "100%" });
        $(".pops").fadeIn();
    });

    $(".main").on("click", ".cls-pop", function () {
        $(".pops").fadeOut();
    });
    //close when press esc
    main.on("keyup", ".opop", function (e) {
        if (e.keyCode == ESC_KEY_CODE) {
            $(".pops").fadeOut();
        }
    });
    //enter btn on search page
    main.on("keyup", ".inner-search", function (e) {
        if (e.keyCode == NTER_KEY_CODE) {
            var searchVal = $(".inner-search").val();
            $(".search-box").val(searchVal);
            var valu = $(".search-box").text(searchVal).css({ "color": "white" });
            $(".inner-search").val();
            $(".pops").fadeOut();
        }
    });
});