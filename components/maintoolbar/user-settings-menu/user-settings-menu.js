$(document).ready(function () {
  $(".main").on("click", ".fa-ellipsis-h", function toggleUserMenu() {
    $("#myDropdown").toggle("show");
    $("#triangle").toggle("show");
  })
  // Close the dropdown if the user clicks outside of it
  $(document).click(function (event) {
    if (!event.target.matches(".fa-ellipsis-h")) {
      $(".user-menu-content").hide();
      $("#triangle").hide();
    }
  });
});