$(document).ready(function () {
    var myStars = ".star";
    var rightArea = $(".right-area");
    rightArea.on("mouseover", myStars, function () {
        var select = $(this);
        select.prevAll().addClass("active-star").removeClass("default-star");
        select.addClass("active-star").removeClass("default-star");
    });

    rightArea.on("click", myStars, function () {
        var select = $(this);
        select.prevAll().removeClass("clicked");
        select.nextAll().removeClass("clicked");
        select.addClass("clicked");
        select.prevAll().addClass("fa-star").removeClass("fa-star-o");
        select.nextAll().addClass("fa-star-o").removeClass("fa-star").removeClass("active-star").addClass("default-star");
        select.addClass("fa-star").removeClass("fa-star-o");
    });

    rightArea.on("mouseleave", myStars, function () {
        var select = $(this);
        var parentStar = select.parent();
        var clickedStar = parentStar.children(".clicked");
        if (clickedStar.length == 1) {
            clickedStar.prevAll().addClass("active-star").removeClass("default-star");
            clickedStar.nextAll().addClass("default-star").removeClass("active-star");
            clickedStar.addClass("active-star").removeClass("default-star");
        }
        else {
            select.prevAll().addClass("default-star");
            select.addClass("default-star");
            select.prevAll().removeClass("active-star");
            select.removeClass("active-star");
        }
    });

});