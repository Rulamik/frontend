$(document).ready(function () {
    $(".hidden-area").hide();
    var expand = "Expand all";
    var collapse = "collapse all";
    var hideShow = $(".hidden-area");
    var toggleIcon = $(".toggle");
    var main = $(".main-content");
    main.on("click", ".toggle", function () {
        var parent = $(this).parents("li");
        var hiddenSection = parent.find(".hidden-area");
        var btn = parent.find("video-info");
        if (hiddenSection.is(":hidden")) {
            $(this).removeClass("fa-chevron-down");
            $(this).addClass("fa-chevron-up");
            hiddenSection.show();
            $(".course-expand").text(expand);
        }
        else {
            $(this).addClass("fa-chevron-down");
            $(this).removeClass("fa-chevron-up");
            hiddenSection.hide();
            $(".course-expand").text(expand);
        }
    });
    main.on("mouseover", ".toggle", function () {
        $(this).css({
            "color": "white",
            "cursor": "pointer"
        });
    });
    main.on("mouseleave", ".toggle", function () {
        $(this).removeAttr("style");

    });
    main.on("click", ".course-expand", function () {
        var parent = $(this).parent();
        if ($(this).html() == expand) {
            $(this).html(collapse);
            hideShow.show();
            toggleIcon.removeClass("fa-chevron-down");
            toggleIcon.addClass("fa-chevron-up");
        }
        else {
            $(this).text(expand);
            hideShow.hide();
            toggleIcon.removeClass("fa-chevron-up");
            toggleIcon.addClass("fa-chevron-down");
        }
    });
    main.on("mouseover", ".course-path", function () {
        $(this).css({
            "color": "gray",
            "cursor": "pointer"
        });
    });
    main.on("mouseover", ".tabs .tab-links span", function () {
        $(this).css({
            "color": "gray",
            "cursor": "pointer"
        });
    });
    main.on("mouseleave", ".tabs .tab-links span", function () {
        $(this).css({
            "color": "white"
        });
    });
    main.on("click", ".tabs .tab-links span", function (e) {
        var currentAttr = $(this).attr("tab");
        var parent = $(this).parent();
        var tabArea = $(document.getElementById(currentAttr));
        // Show/Hide Tabs
        $(tabArea).show().siblings().hide();
        // Change/remove current tab to active
        $(parent).addClass("active").siblings().removeClass("active");
        e.preventDefault();
    });
});