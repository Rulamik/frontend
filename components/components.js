$(document).ready(function () {
    $("library-component").load("./components/middle-area/library/library.html");
    $("toolbar").load("./components/maintoolbar/maintoolbar.html");
    $("course-description").load("./components/middle-area/course-intro/course-intro.html");
    $("table-cont").load("./components/tale-content/table-content.html");
    $("right-menu").load("./components/right-menu/right-menu.html");
    $("tabs-section").load("./components/middle-area/tabs-section/tabs.html");
}); 
